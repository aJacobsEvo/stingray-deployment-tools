#!/bin/bash
#
# MySQL client connected to Stingray database
#

export MYSQL_USR=`grep 'db.user' /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_PWD=`grep 'db.pwd'  /opt/src/stingray/deploy.properties | cut -d "=" -f2-`

mysql stingray -u $MYSQL_USR --pager="most"
