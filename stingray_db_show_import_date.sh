#
# Stingray
#
# Purpose: Display MySQL schema creation date (import)
#

#!/bin/bash

export MYSQL_USR=`grep 'db.user' /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_PWD=`grep 'db.pwd'  /opt/src/stingray/deploy.properties | cut -d "=" -f2-`

mysql stingray -u $MYSQL_USR -e "select table_schema, table_name, create_time from information_schema.tables where table_name like 'policy';"