#!/bin/bash
# ----------------------------------
# Import Stingray database (backup)
# ----------------------------------
set -e

# Init
export MYSQL_USR=`grep 'db.user' /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_PWD=`grep 'db.pwd'  /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_SCHEMA=stingray
export MYSQL_DUMP=$1

# Check dump file (1st param)
if [ -z "$1" ]
then
  echo "Usage:"
  echo "  $0 stingray-db-XYZ.gz [schema-name]"
  exit 1
fi

# Schema name (2nd param, optional)
if [ ! -z "$2" ]
then
  export MYSQL_SCHEMA=$2
fi

echo "~~~ Import..."
echo "MySQL database import:"
echo "  > localost:3306/$MYSQL_SCHEMA"
echo "  from:"
echo "  > $MYSQL_DUMP"

pv "$MYSQL_DUMP" | zcat | mysql $MYSQL_SCHEMA -u $MYSQL_USR
