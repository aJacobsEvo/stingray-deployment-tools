#!/bin/bash
#
# STINGRAY build & deploy
#   * clean build;
#   * run tests;
#   * date / hash of last commit + link to changelog (footer);
#
# Author: Dan Dragut <dan.dragut@endava.com>

# Constants
NORMAL='\e[0m'
GREEN='\e[32m'
YELLOW='\e[33m'
CYAN='\e[36m'
LIGHT_GREEN='\e[92m'
LIGHT_YELLOW='\e[93m'
LIGHT_CYAN='\e[96m'
WHITE='\e[97m'

# Parameters
BRANCH=
TESTS=true
for PARAM in "$@"
do
  case "$PARAM" in
    -?|--help)
      echo
      echo "Usage:"
      echo "  $0 [OPTION...] [BRANCH]"
      echo
      echo "    --no-tests  Build without running tests (equivalent to 'gradle -x test')"
      echo "    --help      Display this help"
      echo
      exit 0
      ;;
    --no-tests)
      TESTS=false
      ;;
    *)
      BRANCH=$PARAM
  esac
done

# Action
clear
printf "${LIGHT_GREEN}%s${NORMAL} [ %s ]\n" "$(hostname)" "$(hostname -I)"
date -R
echo

printf "###############################################\n"
printf "                      ${LIGHT_GREEN}%s${NORMAL}\n" "Wordings"
printf "###############################################\n"
cd /opt/src/wordings
printf "%s [ ${LIGHT_YELLOW}%s${NORMAL} ]\n" "$(pwd)" "$(pwd -P)"

echo
echo ~~~ Remote:
git remote -v | grep "fetch"
echo
echo ~~~ Branch / commit:  [BEFORE]
printf "${YELLOW}%s${NORMAL}\n" "$(git branch | grep "^*")"
printf "${CYAN}%s${NORMAL}\n"   "$(git --no-pager log --pretty=format:"%ai  %h %<(13)%an   %s" -n 1)"

echo
echo ~~~ Pull...
git pull
if [ "$?" -ne "0" ]; then
  exit 11
fi

echo
printf "~~~ Branch / commit:  [${WHITE}AFTER${NORMAL}]\n"
printf "${LIGHT_YELLOW}%s${NORMAL}\n" "$(git branch | grep "^*")"
printf "${LIGHT_CYAN}%s${NORMAL}\n"   "$(git --no-pager log --pretty=format:"%ai  %h %<(13)%an   %s" -n 1)"
echo

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Repository / Branch / Changelog info 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
WORDINGS_GIT_REPO_NAME=`basename $(git config --get remote.$(git config --get branch.master.remote).url) .git`
WORDINGS_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
WORDINGS_GIT_COMMIT_HASH=`git --no-pager log --pretty=format:%h -n 1`
WORDINGS_INFO=$({
    echo "###############################################"
    echo "                      Wordings                 "
    echo "###############################################"
    echo "$(pwd) [ $(pwd -P) ]"
    echo " "
    echo "~~~ Remote:"
    echo "$(git remote -v | grep "fetch")"
    echo " "
    echo "~~~ Branch:"
    echo "$(git branch | grep "* ")"
    echo " "
    echo "~~~ Changes:"
    echo "$(git --no-pager log --pretty=format:"%h  %<(20)%an  %ai  %s" -n 7)"
})
# END of Policy Wordings

echo
printf "###############################################\n"
printf "                      ${LIGHT_GREEN}%s${NORMAL}\n" "Stingray"
printf "###############################################\n"
cd /opt/src/stingray
printf "%s [ ${LIGHT_YELLOW}%s${NORMAL} ]\n" "$(pwd)" "$(pwd -P)"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Gradle
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -x "./gradlew" ] ; then
  GRADLE=./gradlew
else
  GRADLE=gradle
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Show current branch / status...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
echo ~~~ Remote:
git remote -v | grep "fetch"
echo
echo ~~~ Branch / commit:  [BEFORE]
printf "${YELLOW}%s${NORMAL}\n" "$(git branch | grep "^*")"
printf "${CYAN}%s${NORMAL}\n"   "$(git --no-pager log --pretty=format:"%ai  %h %<(13)%an   %s" -n 1)"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Change branch (optional)...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -n "$BRANCH" ]
then
  echo
  read -p "Switch branch to '$BRANCH' [y/n]? " -n 1 -r
  echo
  case "$REPLY" in
    y|Y) echo $'\n'
         git fetch
         git checkout "$BRANCH"
         if [ "$?" -ne "0" ]; then
           exit 1
         fi
         ;;
    *)   echo $'Aborting.\n'
         exit 2
         ;;
  esac
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Pull changes...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
echo '~~~ Pull...'
git pull
if [ "$?" -ne "0" ]
then
  exit 3
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Show current branch...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
printf "~~~ Branch / commit:  [${WHITE}AFTER${NORMAL}]\n"
printf "${LIGHT_YELLOW}%s${NORMAL}\n" "$(git branch | grep "^*")"
printf "${LIGHT_CYAN}%s${NORMAL}\n"   "$(git --no-pager log --pretty=format:"%ai  %h %<(13)%an   %s" -n 1)"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Show branch / last commit in footer
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
STINGRAY_GIT_REPO_NAME=`basename $(git config --get remote.$(git config --get branch.master.remote).url) .git`
STINGRAY_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
STINGRAY_GIT_COMMIT_HASH=`git --no-pager log --pretty=format:%h -n 1`
STINGRAY_INFO=$({
    echo "###############################################"
    echo "                      Stingray                 "
    echo "###############################################"
    echo "$(pwd) [ $(pwd -P) ]"
    echo " "
    echo "~~~ Remote:"
    echo "$(git remote -v | grep "fetch")"
    echo " "
    echo "~~~ Branch:"
    echo "$(git branch | grep "* ")"
    echo " "
    echo "~~~ Changes:"
    echo "$(git --no-pager log --pretty=format:"%h  %<(20)%an  %ai  %s" -n 300)"
})

BUILD_DATE_TIME=`date -R`
# changelog.txt
{
  echo   $BUILD_DATE_TIME
  echo   " "
  printf "${WORDINGS_INFO}"
  echo   " "
  echo   " "
  printf "${STINGRAY_INFO}"
} > web/web/changelog.txt

# footer.jsp
git checkout web/WEB-INF/jspf/foot.jsp
sed -i 's/<div id="serverId">/<div id="serverId"><div style="float: right; vertical-align: middle; padding: 10px"><a href="\/stingray\/web\/changelog.txt" target="changelog" style="font: 9pt monospace; font-weight: bold; color: #ffffff;">'"$STINGRAY_GIT_REPO_NAME \/ $STINGRAY_GIT_BRANCH"' '"$STINGRAY_GIT_COMMIT_HASH"' | '"$WORDINGS_GIT_REPO_NAME \/ $WORDINGS_GIT_BRANCH"' '"$WORDINGS_GIT_COMMIT_HASH"' | '"$BUILD_DATE_TIME"'<\/a><\/div>/g' web/WEB-INF/jspf/foot.jsp  

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Clean / Tests...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
echo
if [ "$TESTS" = false ];
then
    echo '~~~ Clean (No Tests)...'
    $GRADLE --no-daemon clean cleanTest
    if [ "$?" -ne "0" ]
    then
      exit 4
    fi
else
    echo '~~~ Clean & Tests...'
    $GRADLE --no-daemon clean cleanTest test
    if [ "$?" -ne "0" ]
    then
      exit 4
    fi
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Deploy...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
echo '~~~ Build & Deploy...'
$GRADLE --no-daemon release -b deploy.gradle
if [ "$?" -ne "0" ]
then
  exit 5
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Cleanup
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rm -f web/web/changelog.txt
git checkout web/WEB-INF/jspf/foot.jsp

printf "\n"
printf "###############################################\n"
printf "                      ${LIGHT_GREEN}%s${NORMAL}\n" "Mantaray"
printf "###############################################\n"

cd /opt/mantaray/biserver-7.0/
printf "%s [ ${LIGHT_YELLOW}%s${NORMAL} ]\n" "$(pwd)" "$(pwd -P)"

echo
echo '~~~ Restart...'
./start-pentaho.sh
echo

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Change directory back...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~-
