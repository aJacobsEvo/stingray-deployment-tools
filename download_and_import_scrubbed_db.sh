#!/bin/bash
# ------------------------------------------
# Stingray
#  * download scrubbed database (encrypted)
#  * decrypt
#  * import database
#
# ------------------------------------------
set -e

# Parameters
export PROGRESS=true
export BACKUP=true
for PARAM in "$@"
do
  case "$PARAM" in
    --help) 
      echo
      echo "Usage:"
      echo "  $0 [OPTION...]"
      echo
      echo "    --no-progress  Do not show progress (suitable for running with Jenkins)"
      echo "    --no-backup    Do not export database before import"
      echo
      exit 0
      ;; 
    --no-progress)
      export PROGRESS=false
      ;;
    --no-backup)
      export BACKUP=false
      ;;
  esac
done

#
clear
echo START  `date -R`

# Init
export SCRUBBED_GZ=stingray-scrubbed-`date --iso-8601`.gz
export SCRUBBED_GZ_CRYPT=$SCRUBBED_GZ.crypt

# Download & decrypt...
echo
if [ -f "$SCRUBBED_GZ" ];
then
  echo ~~~ Download found:
  ls -la "$SCRUBBED_GZ" --color=always
  echo
else
  if [ "$PROGRESS" = "false" ]; then
    export WGET_PROGRESS=-nv
  else
    export WGET_PROGRESS=
  fi

  echo ~~~ Download...
  wget https://pen-mi.ajginternational.com/scrubbed.gz.crypt -O "$SCRUBBED_GZ_CRYPT" $WGET_PROGRESS
  openssl aes-256-cbc -d -pass "pass:DMgUnHY9PB$8u#B8LzfJ8^vl1VDLvSTWGDt&" -in "$SCRUBBED_GZ_CRYPT" > "$SCRUBBED_GZ"
  rm -f $SCRUBBED_GZ_CRYPT
fi

# Export database (backup)...
if [ "$BACKUP" = true ]; then
  `dirname "$BASH_SOURCE"`/stingray_export_database.sh
  if [ "$?" -ne "0" ]; then
    exit $?
  fi
  echo
fi

# Import database...
`dirname "$BASH_SOURCE"`/stingray_import_database.sh $SCRUBBED_GZ

# Finish
echo
echo FINISH `date -R`
