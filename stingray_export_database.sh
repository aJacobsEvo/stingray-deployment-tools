#!/bin/bash
# ---------------------------------
# Export (backup) Stingray database
# ---------------------------------
set -e

# Init...
export MYSQL_USR=`grep 'db.user' /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_PWD=`grep 'db.pwd'  /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_SCHEMA=stingray

# Schema name (1st param, optional)
if [ ! -z "$1" ]
then
  export MYSQL_SCHEMA=$1
fi

# Dump file...
export MYSQL_DUMP=$MYSQL_SCHEMA-db-`hostname --short`-`date +"%Y%m%d-%H%M%S"`.gz

# Display...
echo "~~~ Export (backup)..."
echo "MySQL database dump:"
echo "  > localost:3306/$MYSQL_SCHEMA"
echo "  to"
echo "  > $MYSQL_DUMP"

# Check schema...
mysql $MYSQL_SCHEMA -u $MYSQL_USR -e "SHOW DATABASES LIKE '$MYSQL_SCHEMA';" > /dev/null

# Dump + compress...
mysqldump $MYSQL_SCHEMA -u $MYSQL_USR --single-transaction --routines --triggers --hex-blob --quick | pv --progress --timer --eta --rate --size 3000m | gzip > $MYSQL_DUMP

# Result...
echo
ls -la $MYSQL_DUMP --color=always

# THE END
