#!/bin/bash
#
# STINGRAY Renewal Processor
#
# Author: Dan Dragut <dan.dragut@endava.com>

# Constants
YELLOW='\033[1;33m'
LIGHT_GREEN='\033[1;32m'
NO_COLOR='\033[0m'

# Init
set -e
if [ ! -z "$TERM" ]; then
  clear
fi

printf "${LIGHT_GREEN}%s${NO_COLOR} [ %s ]\n" "$(hostname)" "$(hostname -I)"
date -R
echo

printf "###############################################\n"
printf "                     ${LIGHT_GREEN}Stingray${NO_COLOR}\n"
printf "###############################################\n"
cd /opt/src/stingray
printf "%s [ ${YELLOW}%s${NO_COLOR} ]\n" "$(pwd)" "$(pwd -P)"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Gradle
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -x "./gradlew" ] ; then
  GRADLE=./gradlew
else
  GRADLE=gradle
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Show current status...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
echo '~~~ Remote:'
git remote -v
echo
echo '~~~ Branch / commit:'
printf "${YELLOW}%s${NO_COLOR}\n" "$(git branch | grep "^*")"
git --no-pager log --pretty=format:"%ai  %h %<(13)%an   %s" -n 1
echo

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Prepare renewal task parameters (optional)...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export RENEWAL_ARGS=
if [ "$#" -gt 0 ]; then
  for PARAM in "$@"
  do
    if [ ! -z "$RENEWAL_ARGS" ]; then
      export RENEWAL_ARGS="$RENEWAL_ARGS, '$PARAM'"
    else
      export RENEWAL_ARGS="'$PARAM'" 
    fi
  done
  export RENEWAL_ARGS="[ $RENEWAL_ARGS ]"
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Run renewal...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo
echo "~~~ Renewal... $RENEWAL_ARGS"
if [ ! -z "$RENEWAL_ARGS" ]; then
  $GRADLE --no-daemon renewal -b scheduled.gradle -PTaskArgs="$RENEWAL_ARGS"
else
  $GRADLE --no-daemon renewal -b scheduled.gradle
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Change directory back...
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd -
