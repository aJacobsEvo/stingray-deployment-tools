#
# Stingray
#
# Purpose: Display last PolicyStatus event (aproximate backup date)
#

#!/bin/bash
clear

export MYSQL_USR=`grep 'db.user' /opt/src/stingray/deploy.properties | cut -d "=" -f2-`
export MYSQL_PWD=`grep 'db.pwd'  /opt/src/stingray/deploy.properties | cut -d "=" -f2-`

mysql stingray -u $MYSQL_USR --table -e "select policy.id, policy.number, policy.record_id, record.status, policystatus.eventtime from policystatus left join policy on policy.id = policystatus.policy_id left join record on record.id = policy.record_id order by eventtime DESC limit 5;"

mysql stingray -u $MYSQL_USR --table -e "select id, name, lastLogin from user order by lastLogin DESC limit 5;"
